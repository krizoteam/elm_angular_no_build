# Withoutbuild

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Examples:

See ./src/app/app.component.html
    ./src/app/app.component.ts
    ./src/elm/src

## Elm Make:

To make the .elm files into regular js:
navigate to ./src/elm

run: elm make src/Main.elm --optimize --output=elm.js

in order to uglify:

uglifyjs elm.js --compress 'pure_funcs="F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9",pure_getters,keep_fargs=false,unsafe_comps,unsafe' | uglifyjs --mangle --output=elm.min.js

## Makefile: 

Does not support uglify

syntax:

make

## NOTES:

Couldnt make the webpack build to run elm make directly from the angular.js file.
Decided to not spend time on it right now.
If needed I can do a custom_webpack_build.js that will replace ./src/elm/compile.sh

See the ./Makefile and ./src/elm/compile.sh

In the ./src/elm folder I ran elm init in order to have everything working nicely.

Resource for future elm-webpack-loader and more examples on how to use multiple elm modules:
https://github.com/elm-community/elm-webpack-loader