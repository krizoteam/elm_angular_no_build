import { Component } from '@angular/core';
import {AfterContentInit} from '@angular/core';
import { Elm } from "../elm/elm.js";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'withoutbuild';
  ngAfterContentInit() {
    const what = Elm.Main.init({node: document.getElementById('elm')});
    what.ports.toElm.send("ok, hello 2");
    what.ports.toJS.subscribe(function(data){
      console.log(data);
    })
  }
}
