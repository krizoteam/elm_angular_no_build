port module Main exposing (..)
import Browser
import Html exposing (Html, div, input, text, button, ul, li)
import Html.Events exposing (onClick, onInput)
import Html.Attributes exposing (type_, placeholder, value)


main =
    Browser.element { init = init
        , subscriptions = subscriptions
        , update = update
        , view = view
        }


type alias Model =
    { input : String
    , messages : List String
    }


type Msg
    = InputChange String
    | SendToJS
    | NewMessage String


init : () ->  (Model, Cmd Msg)
init _ =
    (Model "" [], Cmd.none)


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        InputChange newInput ->
            ({ model | input = newInput }, Cmd.none)
        SendToJS ->
            ({ model | input = "" }, toJS model.input)

        NewMessage incoming ->
            ({ model | messages = model.messages ++ [incoming] }, Cmd.none)


subscriptions : Model -> Sub Msg
subscriptions model =
    toElm NewMessage

view : Model -> Html Msg
view model =
    div []
        [ viewMessages model.messages
        , input [ type_ "text"
                , placeholder "Type a message"
                , onInput InputChange
                , value model.input
                ] []
        , button [ onClick SendToJS ] [ text "Send to JS" ]
        ]


viewMessages : List String -> Html Msg
viewMessages messages =
    ul []
        (List.map viewMessage messages)

viewMessage : String -> Html Msg
viewMessage message =
    li []
        [ text message ]


port toJS : String -> Cmd msg
port toElm : (String -> msg) -> Sub msg