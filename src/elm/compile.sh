#!/bin/bash
for file in ./src/*; do
  filename="${file%.*}"
  elm make "${file}" --optimize --output="$(basename ${filename}).js"
done