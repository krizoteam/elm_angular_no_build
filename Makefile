ELM_PATH = ./src/elm/compile.sh
ELM_DIR = ./src/elm/

default:
	cd ${ELM_DIR} && bash './compile.sh'
	ng serve
